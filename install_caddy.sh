#!/usr/bin/env bash

set -e

BUILD_DIR=$(pwd)/caddy_server
CACHE_DIR=$(pwd)/caddy_download_cache
TMP_PATH="$BUILD_DIR/tmp"
BIN_PATH="$BUILD_DIR/bin"

caddy_bin="caddy"

CADDY_URL="https://caddyserver.com/download/linux/amd64?license=personal&telemetry=off"
CADDY_PKG="$CACHE_DIR/caddy.tar.gz"
CADDY_PATH="$TMP_PATH/caddy"
EXE_PATH="$BIN_PATH/caddy"

mkdir -p $BUILD_DIR $CACHE_DIR
mkdir -p $TMP_PATH $CADDY_PATH $BIN_PATH

if [ -f $EXE_PATH ]; then
  $EXE_PATH --version
fi

echo "-----> Downloading Caddy package"
curl -fsSL "$CADDY_URL" -o "$CADDY_PKG"

echo "-----> Unpacking Caddy package"
tar -zxf "$CADDY_PKG" -C "$CADDY_PATH" "$caddy_bin"
chmod +x "$CADDY_PATH/$caddy_bin"
mv "$CADDY_PATH/$caddy_bin" $BIN_PATH/
rm -rf $CADDY_PATH

version="$($EXE_PATH --version)"
echo "-----> Running Caddy $version"