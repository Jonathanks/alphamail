// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  gmailAuth: {
    apiKey: "AIzaSyBR6IINviVHMc-pjGxU5kTo6PbJLxhi_ns",
    clientId: "801696244263-bd2cegrcq6mo2h60akk6imfunlo0h6n5.apps.googleusercontent.com",
    discoveryUrl: "https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest",
    redirectUri: "https://mailfilters.herokuapp.com",
    scope: [
      "https://www.googleapis.com/auth/gmail.settings.basic",
      "https://www.googleapis.com/auth/gmail.labels",
      "profile",
      "email",
      "openid",
    ],
  },
  outlook: {
    clientId: "68a2407d-2a7f-419a-b4b6-6561b9c97f03",
    oauthScopes: ["MailboxSettings.ReadWrite", "openid", "email", "profile", "User.Read"],
    redirectUri: "https://mailfilters.herokuapp.com",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
