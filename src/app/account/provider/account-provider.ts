import { Account, ProviderName } from "../account";

export const name = Symbol("providerName");

export interface AccountProvider {
  [name]: ProviderName;
  login(): Promise<Account>;
  logout(): Promise<void>;
  getUser(): Promise<Account>;
  getAccessToken(): Promise<string>;
}
