import { Component, OnInit } from "@angular/core";
import { AccountService } from "../account/account.service";
import { Account } from "../account/account";

@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.css"],
})
export class NavigationComponent implements OnInit {
  constructor(public accounts: AccountService) {}
  selectedAccount: Account = null;

  ngOnInit() {
    this.accounts.listenForAccountEvents({
      onAccountSelected: account => {
        this.selectedAccount = account;
      },
    });
  }
}
