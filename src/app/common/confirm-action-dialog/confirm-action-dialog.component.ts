import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "app-confirm-action-dialog",
  templateUrl: "./confirm-action-dialog.component.html",
  styleUrls: ["./confirm-action-dialog.component.css"],
})
export class ConfirmActionDialogComponent implements OnInit {
  @Input() message: {
    title: string;
    details: string;
    options: string[];
  };
  @Input() modal: { visible: boolean };
  @Output() choice = new EventEmitter<{ selectedOption: string }>();

  ngOnInit() {}

  outputSelection(selectedOption: string) {
    this.choice.emit({ selectedOption });
  }

  toggleModal() {
    this.modal.visible = !this.modal.visible;
  }
}
