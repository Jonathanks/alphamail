import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { MsalModule } from "@azure/msal-angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavigationComponent } from "./navigation/navigation.component";
import { AccountComponent } from "./account/account.component";
import { Gmail } from "./account/provider/gmail";
import { Outlook } from "./account/provider/outlook";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AccountService } from "./account/account.service";
import { environment } from "src/environments/environment";
import { FilterFormComponent } from "./filters/filter-form/filter-form.component";
import { FilterRuleComponent } from "./filters/filter-form/filter-rule/filter-rule.component";
import { FiltersComponent } from "./filters/filters.component";
import { FilterManager } from "./filters/managers/filter-manager";
import { OutlookFilterManager } from "./filters/managers/outlook-filter-manager";
import { FilterService } from "./filters/managers/filter-manager.interface";
import { GmailFilterManager } from "./filters/managers/gmail-filter-manager.service";
import { HomeComponent } from "./home/home.component";
import { ConfirmActionDialogComponent } from "./common/confirm-action-dialog/confirm-action-dialog.component";

export function resolveFilterManager(...services: FilterService[]) {
  return new FilterManager(services);
}

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccountComponent,
    FilterFormComponent,
    FilterRuleComponent,
    FiltersComponent,
    HomeComponent,
    ConfirmActionDialogComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    MsalModule.forRoot({
      clientID: environment.outlook.clientId,
      popUp: false,
      isAngular: true,
      redirectUri: environment.outlook.redirectUri,
      postLogoutRedirectUri: environment.outlook.redirectUri,
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    Gmail,
    Outlook,
    GmailFilterManager,
    OutlookFilterManager,
    {
      provide: FilterManager,
      deps: [GmailFilterManager, OutlookFilterManager],
      useFactory: resolveFilterManager,
    },
    AccountService,
  ],
  bootstrap: [AppComponent, NavigationComponent],
  exports: [],
})
export class AppModule {
  constructor() {}
}
