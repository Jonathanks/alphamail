import { Component, OnInit, Input } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-filter-rule",
  templateUrl: "./filter-rule.component.html",
  styleUrls: ["./filter-rule.component.css"],
})
export class FilterRuleComponent implements OnInit {
  constructor() {}

  @Input() form: FormGroup;
  @Input() formGroupName: string;

  ngOnInit() {}
}
