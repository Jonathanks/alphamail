import { Injectable } from "@angular/core";
import { MessageFilter } from "src/schemas/message-filter";
import { clone, pipe, range } from "ramda";
import slug from "slug";

export class FilterRecordConflict extends Error {
  constructor(filter: MessageFilter) {
    super(`A filter with display title '${filter.displayTitle}' already exists in the store.`);
    this.filter = filter;
  }
  filter: MessageFilter;
}

export type StoredFilter = MessageFilter & { dateCreated: Date };

@Injectable({
  providedIn: "root",
})
export class FilterStoreService {
  retrieveAll(): StoredFilter[] {
    const filters = [];
    const addFilterFromLocalstorage = pipe(
      localStorage.key.bind(localStorage),
      localStorage.getItem.bind(localStorage),
      JSON.parse,
      filters.push.bind(filters),
    );
    range(0, localStorage.length).forEach(addFilterFromLocalstorage);
    return filters;
  }

  async remove(key: string) {
    localStorage.removeItem(key);
  }

  async retrieve(key: string): Promise<StoredFilter> {
    return JSON.parse(localStorage.getItem(key), (jsonKey, jsonValue) => {
      if (jsonKey === "dateCreated") {
        return new Date(jsonValue);
      }
      return jsonValue;
    });
  }

  async save(filter: MessageFilter): Promise<StoredFilter> {
    const filterToSave = Object.assign({ dateCreated: new Date() }, clone(filter));
    filterToSave.key = slug(filterToSave.displayTitle, { lower: true });
    const existingFilter = localStorage.getItem(filterToSave.key);
    if (existingFilter) {
      throw new FilterRecordConflict(filterToSave);
    }
    localStorage.setItem(filterToSave.key, JSON.stringify(filterToSave));
    return filterToSave;
  }

  constructor() {}
}
