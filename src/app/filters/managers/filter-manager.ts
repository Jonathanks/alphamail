import { MessageFilter } from "src/schemas/message-filter";
import { Account, ProviderName } from "../../account/account";
import { name } from "src/app/account/provider/account-provider";
import { FilterService, ApplyFilterError } from "./filter-manager.interface";
import { Injectable } from "@angular/core";

@Injectable()
export class FilterManager {
  private services = new Map<ProviderName, FilterService>();
  constructor(services: FilterService[]) {
    services.forEach(service => {
      this.services.set(service[name], service);
    });
  }
  async apply(filter: MessageFilter, account: Account) {
    if (!this.services.has(account.provider)) {
      throw new ApplyFilterError(new Error(`unknown account provider ${account.provider}`), filter, account);
    }
    const service = this.services.get(account.provider);
    await service.apply(filter, account);
  }
}
